package org.example.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.example.models.Region;

public class RegionDao {
	private Connection connection;

	public RegionDao(Connection connection) {
		this.connection = connection;
	}

	public Set<Region> getAll() {
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Set<Region> regions = new HashSet<>();
		try {
			preparedStatement = connection.prepareStatement(
				"SELECT REGION_ID, REGION_NAME FROM REGIONS");

			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				String regionName = resultSet.getString("REGION_NAME");
				int regionId = resultSet.getInt("REGION_ID");
				Region region = new Region(regionName, regionId);
				regions.add(region);
			}
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		} finally {
			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException throwables) {
					throwables.printStackTrace();
				}
			}
			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException throwables) {
					throwables.printStackTrace();
				}
			}
		}
		return regions;
	}

	public Optional<Region> getById(int id) throws SQLException {
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Region region = null;

		preparedStatement = connection.prepareStatement(
			"SELECT REGION_ID, REGION_NAME FROM REGIONS WHERE REGION_ID = ?");
		preparedStatement.setInt(1, id);

		resultSet = preparedStatement.executeQuery();
		if (resultSet.next()) {
			String regionName = resultSet.getString("REGION_NAME");
			int regionId = resultSet.getInt("REGION_ID");
			region = new Region(regionName, regionId);
		}
		return Optional.ofNullable(region);
	}

	public int save(Region region) throws SQLException {
		PreparedStatement preparedStatement = null;

		preparedStatement = connection.prepareStatement(
			"INSERT INTO REGIONS(REGION_NAME) VALUES (?)");
		preparedStatement.setString(1, region.getRegionName());

		return preparedStatement.executeUpdate();
	}

	public int update(Region region) throws SQLException {
		PreparedStatement preparedStatement = null;

		preparedStatement = connection.prepareStatement(
			"UPDATE REGIONS SET REGION_NAME = ? WHERE REGION_ID = ?");
		preparedStatement.setString(1, region.getRegionName());
		preparedStatement.setInt(2, region.getRegionId());

		return preparedStatement.executeUpdate();
	}

	public int delete(int id) throws SQLException {
		PreparedStatement preparedStatement = null;

		preparedStatement = connection.prepareStatement(
			"DELETE FROM REGIONS WHERE REGION_ID = ?");
		preparedStatement.setInt(1, id);

		return preparedStatement.executeUpdate();
	}
}
