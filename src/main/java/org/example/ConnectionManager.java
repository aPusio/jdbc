package org.example;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionManager {
	private String connectionString = "jdbc:hsqldb:file:db-data/mydatabase;user=admin123;password=admin123";
	private String username = "admin123";
	private String password = "admin123";

	public Connection getConnection(){
		Connection connection = null;
		try {
			connection = DriverManager.getConnection(connectionString, username, password);
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}
		return connection;
	}
}
