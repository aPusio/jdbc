package org.example;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.example.dao.RegionDao;
import org.example.models.Region;

public class App {
	public static void main(String[] args) throws Exception {
//        DbCreator dbCreator = new DbCreator();
//        dbCreator.createAndLoadData();
		ConnectionManager connectionManager = new ConnectionManager();
		Connection connection = connectionManager.getConnection();
		RegionDao regionDao = new RegionDao(connection);
//		Optional<Region> region = regionDao.getById(999);
//		region.ifPresent(System.out::println);

//		Region region = new Region("Arctic");
//		int saved = regionDao.save(region);
//		System.out.println("amount of saved records: " + saved);
//		regionDao.getAll().forEach(System.out::println);

//		Optional<Region> regionFromDb = regionDao.getById(5);
//		if(regionFromDb.isPresent()){
//			Region region = regionFromDb.get();
//			region.setRegionName("Aarctic");
//			regionDao.update(region);
//		}
//
//		regionDao.getAll().forEach(System.out::println);

		int delete = regionDao.delete(5);
		System.out.println("amount of deleted rows" + delete);
		regionDao.getAll().forEach(System.out::println);

		connection.close();
	}
}